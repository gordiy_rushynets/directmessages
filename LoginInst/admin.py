from django.contrib import admin
from .models import *


class MessageAdmin(admin.ModelAdmin):
    list_display = ['message',]

    class Meta:
        model = Message

admin.site.register(Message, MessageAdmin)
admin.site.register(UsersIDs)

