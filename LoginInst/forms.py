from django import forms
from .models import Message


# login form for login to instagram profile
class LoginForm(forms.Form):
    login = forms.CharField(label='Login', max_length=200)
    pswd = forms.CharField(label='Password', max_length=200, widget=forms.TextInput(attrs={'placeholder': "Password", 'type': 'password'}))


# message form for sending messages each users in direct
class MessageForm(forms.ModelForm):
    # msg = forms.CharField(label='Message', widget=forms.Textarea(attrs={'placeholder': "Message"}))

    class Meta:
        model = Message
        fields = ['message', 'first_posting',]

