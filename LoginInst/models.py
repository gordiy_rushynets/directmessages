from django.db import models


class Message(models.Model):
    message = models.TextField()
    first_posting = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.message)


class UsersIDs(models.Model):
    user_ids = models.CharField(max_length=160)

    def __str__(self):
        return "{}".format(self.user_ids)

    class Meta:
        verbose_name = "UserID"
        verbose_name_plural = "UserIDs"
