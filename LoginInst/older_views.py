from django.shortcuts import render
from django.http import HttpResponseRedirect

import threading
import os, shutil

import time
from datetime import datetime

from instabot import Bot
from InstagramAPI import InstagramAPI

from .forms import LoginForm, MessageForm
from .models import UsersIDs



login = ""
pswd = ""
msg = ""
count_message = 0
api = None


# Login to instagram
def login_to_inst(request):
    global login
    global pswd
    global api

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid():
            api = InstagramAPI(form.cleaned_data['login'], form.cleaned_data['pswd'])

            if api.login():

                login = form.cleaned_data['login']
                pswd = form.cleaned_data['pswd']
                print("User loged-in.")

                return HttpResponseRedirect('message/')
            else:
                print("Login error!!!")

    else:
        form = LoginForm()

    return render(request, 'instagram/insta_login.html', {'form': form})


# send message
def message_to_direct(request):
    global msg
    global count_message

    if request.method == 'POST':

        form = MessageForm(request.POST)

        if form.is_valid():
            # form.save(commit=True)
            msg = form.cleaned_data['message']
            first_use = form.cleaned_data['first_posting']
            # new_directs = form.cleaned_data['has_new_directs']
            count_message = form.cleaned_data['count_message']

            if first_use == True:
                return HttpResponseRedirect('/first_success/')
            else:
                return HttpResponseRedirect('/success/')
        else:
            print("didn't send message")

    else:
        form = MessageForm()

    return render(request, 'instagram/message.html', {'form': form})

def get_certain_ids(list_ids):
    list3 = []
    list2 = list_ids
    for i in range(len(list2)):
        j = 0
        while list2[i][j] != '=':
            j += 1
        log = list2[i][:j]
        if log == login:
            list3.append(list2[i])

    return list3


def delete_login_from_id(list_string):
    list2 = list_string
    list1 = []
    for str in list2:
        i = 0
        while str[i] != '=':
            list_str = list(str)
            list_str.pop(i)
            str = "".join(list_str)

        list1.append(str)

    list3 = []

    for str in list1:
        list_str = list(str)
        list_str.pop(0)
        str = "".join(list_str)
        list3.append(str)

    return list3

def ids(list_ids):
    list_string = get_certain_ids(list_ids=list_ids)
    ids = delete_login_from_id(list_string=list_string)
    return ids


def clear(f_name):
    f_path = os.getcwd() + "/" + f_name
    if os.path.exists(f_path):
        os.remove(f_path)

    else:
        print('Not removed {0}'.format(f_name))
    return f_path

def sort():
    # thread_ids = open(login + "threads_ids.txt", "r")
    users_ids_file = open(login + 'user_ids.txt', 'r')
    users_ids = []

    for users_id in users_ids_file:
        users_ids.append(users_id)

    temp = 0
    for i in range(0, len(users_ids) - 1):
        for j in range(0,len(users_ids) - i -1):
            if users_ids[j] < users_ids[j+1]:
                temp = users_ids[j]
                users_ids[j] = users_ids[j+1]
                users_ids[j+1] = temp
    index = 0
    ui = []
    for i in users_ids:
        i = i[:-1]
        ui.append(i)

    users_ids = list(set(ui))

    return users_ids


def first_posting():
    global count_message
    start1 = datetime.now()
    print("Start sending messages: ", start1)

    thread_ids_file = open(login + 'threads_ids.txt', 'a')
    users_ids_file = open(login + 'user_ids.txt', 'r')

    api = InstagramAPI(login, password=pswd)

    if api.login():

        bot = Bot()
        bot.login(username=login, password=pswd)

        if count_message >= 700 and count_message < 1400:
            c = int(count_message/40)*1.6
        elif count_message >= 1400 and count_message < 2000:
            c = int(count_message/50)*1.65
        elif count_message >= 2000 and count_message < 4000:
            c = int(count_message/55)*1.6
        elif count_message >= 4000 and count_message < 6000:
            c = int(count_message/70)*1.55
        elif count_message >= 6000 and count_message < 8000:
            c = int(count_message/75)*1.7
        elif count_message >= 8000 and count_message < 11000:
            c = int(count_message/85)*1.7
        elif count_message >= 11000 and count_message < 15000:
            c = int(count_message/95)*1.7
        elif count_message >= 15000 and count_message < 20000:
            c = int(count_message/110)*1.5
            time.sleep(15)
        elif count_message >= 20000 and count_message < 25000:
            c = int(count_message/115)*1.4
            time.sleep(15)
        elif count_message >= 25000 and count_message < 30000:
            c = int(count_message/125)*1.3
            time.sleep(14)
        elif count_message >= 30000 and count_message < 34000:
            c = int(count_message/132)*1.2
            time.sleep(15)
        elif count_message >= 3400 and count_message < 36000:
            c = int(count_message/134)*1.7
            time.sleep(15)
        elif count_message >= 36000 and count_message < 40000:
            c = int(count_message/138)*1.5
            time.sleep(15)
        elif count_message >= 40000 and count_message < 45000:
            time.sleep(15)
            c = int(count_message/142)*1.7
        elif count_message >= 45000 and count_message < 52000:
            c = int(count_message/145)*1.8
            time.sleep(15)

        c = count_message/100
        index = 0
        while index != 200 or index != int(c):
            index += 1
            if index >= 1000 and index % 100 == 0:
                time.sleep(20)

            data = bot.get_messages()

            thread_ids_file = open(login + 'threads_ids.txt', 'a')
            users_ids_file = open(login + 'user_ids.txt', 'a')

            try:
                for i in range(len(data['inbox']['threads'])):
                    thread_id = data['inbox']['threads'][i]['thread_id']
                    item_id = data['inbox']['threads'][i]['items'][0]['item_id']
                    user_id = data['inbox']['threads'][i]['users'][0]['pk']

                    # delete previous message
                    if i >= 0:
                        # 1 generate file threads id
                        thread_ids_file.write(thread_id + '\n')

                        try:
                            UsersIDs.objects.get(user_ids=str(login + "=" + str(user_id)))
                        except:
                            UsersIDs.objects.create(user_ids=str(login + "=" + str(user_id)))

                        # 2 generate list_users_ids
                        users_ids_file.write(str(user_id) + '\n')

                        bot.api.deleteItem(thread_id=thread_id, item_id=item_id)

                        print("Index: ", index)
                        print("i: ", i)

                        time.sleep(2)
                time.sleep(12)
            except:
                break

            print(index)

    u_i = []

    for i in range(len(UsersIDs.objects.all())):
        data = UsersIDs.objects.all()
        u_ids = UsersIDs.objects.get(user_ids=data[i])
        u_i.append(str(u_ids))
        # print(data[i])


    user_ids = ids(list_ids=u_i)
    for i in range(len(user_ids)):
        api.direct_message(msg, user_ids[i])
        print(i+1, ')', user_ids[i])
        time.sleep(2)
        if i >= 1000 and i % 100 == 0:
            time.sleep(25)
        elif i >= 125 and i % 5 == 0:
            time.sleep(10)



    # while index!= count_message - 2:
    #     try:
    #         user_ids = UsersIDs.objects.order_by('user_ids')[index]
    #         u_ids = UsersIDs.objects.get(user_ids=user_ids)
    #         # print(u_ids)
    #         u_i.append(str(u_ids))
    #         index += 1
    #     except:
    #         print("List all users ids formed!")


    # user_ids = ids(list_ids=u_i)
    #
    # # sending message
    # index = 0
    # for user in user_ids:
    #     api.direct_message(msg, user)
    #     print(index, "user_id:", user)
    #     index += 1
    #     time.sleep(2)


    # sending from file
    # users_ids = sort()
    #
    # x = 0
    # for u_i in users_ids:
    #     api.direct_message(msg, str(u_i))
    #     print(x, ') ', str(u_i))
    #     x += 1
    #     time.sleep(2)


def sending():
    start1 = datetime.now()
    print("Start sending messages: ", start1)

    # thread_ids_file = open('threads_ids.txt', 'a')
    # users_ids_file = open('user_ids.txt', 'a')
    # thread_ids = open("threads_ids.txt", "r")


    api = InstagramAPI(login, password=pswd)
    api.login()

    index = 0
    u_i = []
    while True:
        try:
            user_ids = UsersIDs.objects.order_by('user_ids')[index]
            u_ids = UsersIDs.objects.get(user_ids=user_ids)
            # print(u_ids)
            u_i.append(str(u_ids))
            index += 1
        except:
            print("List all users ids formed!")
            break

    user_ids = ids(list_ids=u_i)

    # sending message
    index = 0
    for user in user_ids:
        api.direct_message(msg, user)
        print(index, "user_id:", user)
        time.sleep(2)
        index += 1

        if index >= 1000 and index % 100 == 0:
            time.sleep(50)
        elif index >= 100 and index % 10 == 0:
            time.sleep(13)


    # users_ids = sort()
    #
    # x = 0
    # for u_i in users_ids:
    #     api.direct_message(msg, str(u_i))
    #     print(x, ') ', str(u_i))
    #     x += 1
    #     time.sleep(2)

    cookie = 'cookie.txt'
    print("Removed: ", clear(cookie))
    print("Removed: ", clear(login+'.chechpoint'))

    print("Bot end sending message: ", datetime.now())


def new_directs():
    start1 = datetime.now()
    print("Start sending messages: ", start1)

    api = InstagramAPI(login, password=pswd)

    if api.login():

        bot = Bot()
        bot.login(username=login, password=pswd)

        index = 0
        while True:
            index += 1

            if index == 70 + 15:
                break
            data = bot.get_messages()

            thread_ids_file = open(login + 'threads_ids.txt', 'a')
            users_ids_file = open(login + 'user_ids.txt', 'a')

            try:
                for i in range(len(data['inbox']['threads'])):
                    thread_id = data['inbox']['threads'][i]['thread_id']
                    item_id = data['inbox']['threads'][i]['items'][0]['item_id']
                    user_id = data['inbox']['threads'][i]['users'][0]['pk']

                    # delete previous message
                    if i >= 0:
                        # 1 generate file threads id
                        thread_ids_file.write(thread_id + '\n')

                        try:
                            UsersIDs.objects.get(user_ids=str(login + "=" + str(user_id)))
                        except:
                            UsersIDs.objects.create(user_ids=str(login + "=" + str(user_id)))

                        # 2 generate list_users_ids
                        users_ids_file.write(str(user_id) + '\n')

                        bot.api.deleteItem(thread_id=thread_id, item_id=item_id)

                        print("Index: ", index)
                        print("i: ", i)

                        time.sleep(2)
            except:
                break

            print(index)

    # users_ids = UsersIDs.objects.order_by('user_ids')[0]
    # print(users_ids)
    # users_ids = UsersIDs.objects.order_by('user_ids')[1]
    # print(users_ids)

    index = 0
    u_i = []
    while True:
        try:
            user_ids = UsersIDs.objects.order_by('user_ids')[index]
            u_ids = UsersIDs.objects.get(user_ids=user_ids)
            # print(u_ids)
            u_i.append(str(u_ids))
            index += 1
        except:
            print("List all users ids formed!")
            break

    print(u_i[0])

    user_ids = ids(list_ids=u_i)

    # sending messages
    index = 0
    for user in user_ids:
        api.direct_message(msg, user)
        print(index, "user_id:", user)
        index += 1
        time.sleep(2)


# sending success
def success(request):

    th = threading.Thread(target=sending)
    # th.daemon = True
    th.start()
    return render(request, 'instagram/success.html')


def first_success(request):

    th = threading.Thread(target=first_posting)
    th.start()
    return  render(request, 'instagram/first_success.html')


def have_new_directs(request):
    th = threading.Thread(target=new_directs)
    th.start()
    return render(request, 'instagram/first_success.html')


def other_sending(requets):
    
    fp = os.getcwd()
    messages = os.listdir(fp)
    for msg in messages:
        msg = os.path.join(fp, msg)
        print(msg)
        print('_____--------______')
        if os.path.isfile(msg):
            os.unlink(msg)
        else:
            shutil.rmtree(msg)

    msg1 = "Hello from developer"
    return render(requets, 'instagram/success.html', {'msg': msg1})


# infrormation about site
def inform(request):
    return render(request, 'instagram/information.html')

