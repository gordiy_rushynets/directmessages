from django.urls import path
from LoginInst import older_views, views

# urlpatterns = [
#     path('', older_views.login_to_inst, name='login'),
#     path('message/', older_views.message_to_direct, name='message'),
#     path('success/', older_views.success, name='success'),
#     path('first_success/', older_views.first_success, name='first_success'),
#     path('other_success/', older_views.other_sending, name='success'),
#     path('inform/', older_views.inform, name='information'),
# ]


urlpatterns = [
    path('', views.login_to_inst, name='login'),
    path('message/', views.message_to_direct, name='message'),
    path('success/', views.success, name='success'),
    path('first_success/', views.first_success, name='first_success'),
    path('other_success/', views.other_sending, name='other_success'),
]