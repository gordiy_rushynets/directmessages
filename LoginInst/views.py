from django.shortcuts import render
from django.http import HttpResponseRedirect

import threading
import os, shutil

import time
from datetime import datetime

from instabot import Bot
from InstagramAPI import InstagramAPI

from .forms import LoginForm, MessageForm
from .models import UsersIDs



login = ""
pswd = ""
msg = ""
api = None

# Login to instagram
def login_to_inst(request):
    global login
    global pswd
    global api

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid():
            api = InstagramAPI(form.cleaned_data['login'], form.cleaned_data['pswd'])

            if api.login():

                login = form.cleaned_data['login']
                pswd = form.cleaned_data['pswd']
                print("User loged-in.")

                return HttpResponseRedirect('message/')
            else:
                print("Login error!!!")

    else:
        form = LoginForm()

    return render(request, 'instagram/insta_login.html', {'form': form})

# send message
def message_to_direct(request):
    global msg

    if request.method == 'POST':

        form = MessageForm(request.POST)

        if form.is_valid():
            # form.save(commit=True)
            msg = form.cleaned_data['message']
            first_use = form.cleaned_data['first_posting']
            if first_use == True:
                return HttpResponseRedirect('/first_success/')
            else:
                return HttpResponseRedirect('/success/')

        else:
            print("didn't send message")

    else:
        form = MessageForm()

    return render(request, 'instagram/message.html', {'form': form})


def get_certain_ids(list_ids):
    list3 = []
    list2 = list_ids
    for i in range(len(list2)):
        j = 0
        while list2[i][j] != '=':
            j += 1
        log = list2[i][:j]
        if log == login:
            list3.append(list2[i])

    return list3

def delete_login_from_id(list_string):
    list2 = list_string
    list1 = []
    for str in list2:
        i = 0
        while str[i] != '=':
            list_str = list(str)
            list_str.pop(i)
            str = "".join(list_str)

        list1.append(str)

    list3 = []

    for str in list1:
        list_str = list(str)
        list_str.pop(0)
        str = "".join(list_str)
        list3.append(str)

    return list3

def ids(list_ids):
    list_string = get_certain_ids(list_ids=list_ids)
    ids = delete_login_from_id(list_string=list_string)
    return ids


def first_posting():
    bot = Bot()
    bot.login(username=login, password=pswd)

    api = InstagramAPI(login, pswd)
    api.login()

    user_ids = []

    try:
        user_ids = bot.get_all_messages()
        print("Function get list ids from get all messages")
    except:
        data = bot.get_messages()
        user_ids = []
        for i in range(len(data['inbox']['threads'])):
            user_ids.append(data['inbox']['threads'][i]['users'][0]['pk'])
        print("formed list ids from get messages")

    for i in range(len(user_ids)):
        try:
            UsersIDs.objects.get(user_ids=str(login + "=" + str(user_ids[i])))
            print("Created before in db: ", user_ids[i])
        except:
            UsersIDs.objects.create(user_ids=str(login + "=" + str(user_ids[i])))
            print("Adding to db: ", user_ids[i])
        print(i, user_ids[i])

    u_i = []

    for i in range(len(UsersIDs.objects.all())):
        data = UsersIDs.objects.all()
        u_ids = UsersIDs.objects.get(user_ids=data[i])
        u_i.append(str(u_ids))


    users_ids = ids(list_ids=u_i)

    for i in range(len(users_ids)):
        api.direct_message(msg , users_ids[i])
        time.sleep(2)
        print('--------------')
        print(i + 1, users_ids[i])
        if i % 10 == 0:
            time.sleep(3)
        elif i % 100 == 0:
            time.sleep(15)
        elif i % 1000 == 0:
            time.sleep(60*2)


def sending():
    api = InstagramAPI(login, pswd)
    api.login()

    u_i = []

    for i in range(len(UsersIDs.objects.all())):
        data = UsersIDs.objects.all()
        u_ids = UsersIDs.objects.get(user_ids=data[i])
        u_i.append(str(u_ids))

    users_ids = ids(list_ids=u_i)

    for i in range(len(users_ids)):
        api.direct_message(msg, users_ids[i])
        time.sleep(2)
        print(i + 1, users_ids[i])

        if i % 10 == 0:
            time.sleep(3)
        elif i % 100 == 0:
            time.sleep(15)
        elif i % 1000 == 0:
            time.sleep(60 * 2)

# sending success
def success(request):

    th = threading.Thread(target=sending)
    # th.daemon = True
    th.start()
    return render(request, 'instagram/success.html')


# first posting message with service
def first_success(request):

    th = threading.Thread(target=first_posting)
    th.start()
    return  render(request, 'instagram/first_success.html')


def other_sending(requets):
    fp = os.getcwd()
    messages = os.listdir(fp)
    for msg in messages:
        msg = os.path.join(fp, msg)
        print(msg)
        print('_____--------______')
        if os.path.isfile(msg):
            os.unlink(msg)
        else:
            shutil.rmtree(msg)

    msg1 = "Hello from developer"
    return render(requets, 'instagram/success.html', {'msg': msg1})
